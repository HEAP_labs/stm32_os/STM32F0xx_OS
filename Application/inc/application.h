/**
  * @file    application.h
  * @author  Andreas Hirtenlehner, Gerald Ebmer, Lukas Pechhacker
  * @brief   Header file for the application module
  */

#ifndef __APPLICATION_H
#define __APPLICATION_H

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/** @addtogroup Application
  * @{
  */

/** @addtogroup Tasks 
  * @{
  */

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/** @defgroup Tasks_Exported_Constants
  * @{
  */ 
	
/**
  * @brief taskclass period time definitions [s]
  * @{
  */
	
#define T_PERIOD1_S ((double)1e-4)
#define T_PERIOD2_S ((double)1e-3)
#define T_PERIOD3_S ((double)1e-1)
#define T_PERIOD_SOFT_RT_S ((double)1.0)
	
/**
  * @}
  */ 

/**
  * @}
  */ 

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

/**
  * @}
  */ 
	
/**
  * @}
  */ 

#endif // __APPLICATION_H
