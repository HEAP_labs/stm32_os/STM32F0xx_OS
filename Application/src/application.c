/**
  * @file    application.c 
  * @author  Andreas Hirtenlehner, Gerald Ebmer, Lukas Pechhacker
  * @brief   Implementation of the application code
  */

/* Includes ------------------------------------------------------------------*/
#include "application.h"

/** @addtogroup Application
  * @{
  */

/** @defgroup Tasks 
  * @brief realtime tasks
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
void taskclass1(void);
void taskclass2(void);
void taskclass3(void);
void soft_rt_taskclass(void);

/* Private functions ---------------------------------------------------------*/
/** @defgroup Tasks_Private_Functions
  * @{
  */
	
/**
  * @brief  fastest taskclass with highest priority (called periodically)
  */
void taskclass1()
{

#ifdef __USING_WWDG
  WWDG_SetCounter(0x7F);
#endif // __USING_WWDG
}

/**
  * @brief  taskclass (called periodically)
  */
void taskclass2()
{
	
#ifdef __USING_IWDG
  IWDG_ReloadCounter();
#endif // __USING_IWDG
}

/**
  * @brief  taskclass (called periodically)
  */
void taskclass3()
{

}

/**
  * @brief  slowest taskclass (called periodically in taskclass3)
  */
void soft_rt_taskclass()
{

}

/**
  * @brief startup - function, called in the main module
*/
void startup_sequence(void)
{
  SystemCoreClockUpdate();
	
#ifdef __USING_ADC
  g_adc_ref_v = 3.3;
#endif // __USING_ADC
  
#ifdef __USING_DAC
  g_dac_ref_v = 3.3;
#endif // __USING_DAC
  
#ifdef __USING_TIMER
  API_TIM_start(&API_TIM14, TIM_IT_Update, T_PERIOD1_S, 0);
	API_TIM_start(&API_TIM16, TIM_IT_Update, T_PERIOD2_S, 4);
  API_TIM_start(&API_TIM17, TIM_IT_Update, T_PERIOD3_S, 5);
#endif // __USING_TIMER
  
#ifdef __USING_IWDG
  API_IWDG_init(); //updated in Taskclass2, ~0.5s
#endif // __USING_IWDG

#ifdef __USING_WWDG  
  API_WWDG_init(); //updated in Taskclass1, window: 97.5�s - 6.2ms
#endif // __USING_WWDG
}

/**
  * @}
  */

/**
  * @}
  */ 

/**
  * @}
  */ 
