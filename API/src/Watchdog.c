/**
  * @file    ./API/src/Watchdog.c 
  * @author  Andreas Hirtenlehner
  * @brief   API for WWDG and IWDG
  */
  
#include "../inc/PerUsings.h"
#if defined(__USING_WWDG) || defined(__USING_IWDG)

/* Includes ------------------------------------------------------------------*/
#include "Watchdog.h"

/** @addtogroup STM32F0xx_API
  * @{
  */

/** @defgroup Watchdog 
  * @brief Watchdog API functions
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
void API_WWDG_init(void);
void API_IWDG_init(void);

/* Private functions ---------------------------------------------------------*/
/** @defgroup Watchdog_Private_Functions
  * @{
  */
	
/**
  * @brief  init window watchdog.
  */
void API_WWDG_init(void)
{
  DBGMCU_APB1PeriphConfig(DBGMCU_WWDG_STOP, ENABLE);
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_WWDG, ENABLE);
  WWDG_SetPrescaler(WWDG_Prescaler_1);
  WWDG_SetWindowValue(0x7E);
  WWDG_Enable(0x7F);
}

/**
  * @brief  init independent watchdog.
  */
void API_IWDG_init(void)
{
  DBGMCU_APB1PeriphConfig(DBGMCU_IWDG_STOP, ENABLE);
  IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
  IWDG_SetPrescaler(IWDG_Prescaler_4);
  IWDG_SetReload(0x0FFF);
  IWDG_Enable();
}

/**
  * @}
  */

/**
  * @}
  */ 

/**
  * @}
  */ 

#endif // __USING_WWDG || __USING_IWDG
