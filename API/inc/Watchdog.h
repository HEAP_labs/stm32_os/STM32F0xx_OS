/**
  ******************************************************************************
  * @file    ./API/inc/Watchdog.h 
  * @author  Andreas Hirtenlehner
  * @brief   header for Watchdog.c module
  */

#ifndef __WATCHDOG_H
#define __WATCHDOG_H

/* Includes ------------------------------------------------------------------*/
#include "mcu.h"

/** @addtogroup STM32F0xx_API
  * @{
  */

/** @addtogroup Watchdog
  * @{
  */
	
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern void API_WWDG_init(void);
extern void API_IWDG_init(void);

/**
  * @}
  */ 
	
/**
  * @}
  */ 

#endif // __WATCHDOG_H
