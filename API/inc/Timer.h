/**
  * @file    ./API/inc/Timer.h
  * @author  Andreas Hirtenlehner, Gerald Ebmer
  * @brief   Header for Timer.c module
  */

#ifndef __TIMER_H
#define __TIMER_H

/* Includes ------------------------------------------------------------------*/
#include "mcu.h"
#include "GPIO.h"

/** @addtogroup STM32F0xx_API
  * @{
  */

/** @addtogroup Timer
  * @{
  */

/* Exported types ------------------------------------------------------------*/

/** 
  * @brief  Timer Init structure definition
  */
	
typedef struct{
  TIM_TypeDef*  TIMx;
  __IO uint32_t RCC_APBxPeriph_TIMx;
  __IO uint8_t  GPIO_AF_TIMx;
  IRQn_Type     IRQnx;
  uint32_t      period_timer_ticks;
  DMA_Channel_TypeDef* DMAx_Channelx;
} API_TIM_type_t;

/* Exported constants --------------------------------------------------------*/
/** @defgroup Timer_Exported_Constants
  * @{
  */ 

#define CH1 0x01
#define CH2 0x02
#define CH3 0x04
#define CH4 0x08

/**
  * @}
  */ 

#ifdef STM32F030
extern API_TIM_type_t API_TIM1;
extern API_TIM_type_t API_TIM3;
extern API_TIM_type_t API_TIM14;
extern API_TIM_type_t API_TIM15;
extern API_TIM_type_t API_TIM16;
extern API_TIM_type_t API_TIM17;
#endif // STM32F030

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern void API_TIM_start(API_TIM_type_t* API_TIMx, uint16_t tim_it, double t_period_s, uint8_t priority);
extern void API_TIM_stop(API_TIM_type_t* API_TIMx, uint16_t tim_it);
extern void API_PWM_start(API_TIM_type_t* API_TIMx, uint32_t CHx, API_GPIO_type_t* portpin, double t_period_s, double duty_cycle, uint8_t deadtime, uint16_t TIM_OCPolarity);
extern void API_PWM_set_dutycycle(API_TIM_type_t* API_TIMx, uint8_t CHx, double dutycycle);
  
/**
  * @}
  */ 
	
/**
  * @}
  */ 
	
#endif // __TIMER_H
