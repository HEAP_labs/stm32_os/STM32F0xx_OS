/**
  * @file    ./API/inc/GPIO.h 
  * @author  Andreas Hirtenlehner, Gerald Ebmer
  * @brief   Header for GPIO.c module
  */

#ifndef __GPIO_H
#define __GPIO_H

/* Includes ------------------------------------------------------------------*/
#include "mcu.h"

/** @addtogroup STM32F0xx_API
  * @{
  */

/** @addtogroup GPIO
  * @{
  */

/* Exported types ------------------------------------------------------------*/

/** 
  * @brief  GPIO Init structure definition
  */
	
typedef struct
{
	GPIO_TypeDef* GPIOx;
	__IO uint16_t GPIO_Pin_x;
	__IO uint32_t RCC_AHB1Periph_GPIOx;
} API_GPIO_type_t;

/* Exported constants --------------------------------------------------------*/
/** 
  * @brief  exported GPIO ports
  * @{
  */

extern API_GPIO_type_t PA0;
extern API_GPIO_type_t PA1;
extern API_GPIO_type_t PA2;
extern API_GPIO_type_t PA3;
extern API_GPIO_type_t PA4;
extern API_GPIO_type_t PA5;
extern API_GPIO_type_t PA6;
extern API_GPIO_type_t PA7;
extern API_GPIO_type_t PA8;
extern API_GPIO_type_t PA9;
extern API_GPIO_type_t PA10;
extern API_GPIO_type_t PA11;
extern API_GPIO_type_t PA12;
extern API_GPIO_type_t PA13;
extern API_GPIO_type_t PA14; 
extern API_GPIO_type_t PA15;

extern API_GPIO_type_t PB0;
extern API_GPIO_type_t PB1;
extern API_GPIO_type_t PB2;
extern API_GPIO_type_t PB3;
extern API_GPIO_type_t PB4;
extern API_GPIO_type_t PB5;
extern API_GPIO_type_t PB6;
extern API_GPIO_type_t PB7;
extern API_GPIO_type_t PB8;
extern API_GPIO_type_t PB9;
extern API_GPIO_type_t PB10;
extern API_GPIO_type_t PB11;
extern API_GPIO_type_t PB12;
extern API_GPIO_type_t PB13;
extern API_GPIO_type_t PB14; 
extern API_GPIO_type_t PB15;

extern API_GPIO_type_t PC0;
extern API_GPIO_type_t PC1;
extern API_GPIO_type_t PC2;
extern API_GPIO_type_t PC3;
extern API_GPIO_type_t PC4;
extern API_GPIO_type_t PC5;
extern API_GPIO_type_t PC6;
extern API_GPIO_type_t PC7;
extern API_GPIO_type_t PC8;
extern API_GPIO_type_t PC9;
extern API_GPIO_type_t PC10;
extern API_GPIO_type_t PC11;
extern API_GPIO_type_t PC12;
extern API_GPIO_type_t PC13;
extern API_GPIO_type_t PC14; 
extern API_GPIO_type_t PC15;

extern API_GPIO_type_t PD0;
extern API_GPIO_type_t PD1;
extern API_GPIO_type_t PD2;
extern API_GPIO_type_t PD3;
extern API_GPIO_type_t PD4;
extern API_GPIO_type_t PD5;
extern API_GPIO_type_t PD6;
extern API_GPIO_type_t PD7;
extern API_GPIO_type_t PD8;
extern API_GPIO_type_t PD9;
extern API_GPIO_type_t PD10;
extern API_GPIO_type_t PD11;
extern API_GPIO_type_t PD12;
extern API_GPIO_type_t PD13;
extern API_GPIO_type_t PD14; 
extern API_GPIO_type_t PD15;

extern API_GPIO_type_t PE0;
extern API_GPIO_type_t PE1;
extern API_GPIO_type_t PE2;
extern API_GPIO_type_t PE3;
extern API_GPIO_type_t PE4;
extern API_GPIO_type_t PE5;
extern API_GPIO_type_t PE6;
extern API_GPIO_type_t PE7;
extern API_GPIO_type_t PE8;
extern API_GPIO_type_t PE9;
extern API_GPIO_type_t PE10;
extern API_GPIO_type_t PE11;
extern API_GPIO_type_t PE12;
extern API_GPIO_type_t PE13;
extern API_GPIO_type_t PE14; 
extern API_GPIO_type_t PE15;

extern API_GPIO_type_t PF0;
extern API_GPIO_type_t PF1;
extern API_GPIO_type_t PF2;
extern API_GPIO_type_t PF3;
extern API_GPIO_type_t PF4;
extern API_GPIO_type_t PF5;
extern API_GPIO_type_t PF6;
extern API_GPIO_type_t PF7;
extern API_GPIO_type_t PF8;
extern API_GPIO_type_t PF9;
extern API_GPIO_type_t PF10;
extern API_GPIO_type_t PF11;
extern API_GPIO_type_t PF12;
extern API_GPIO_type_t PF13;
extern API_GPIO_type_t PF14; 
extern API_GPIO_type_t PF15;

/**
  * @}
  */ 

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern uint8_t API_getDI(API_GPIO_type_t* portpin);
extern uint8_t API_getActiveDI(API_GPIO_type_t* portpin);
extern uint16_t API_getDI_port(GPIO_TypeDef* GPIOx);

extern void API_setDO(API_GPIO_type_t* portpin, BitAction bit_val);
extern void API_setActiveDO(API_GPIO_type_t* portpin, BitAction bit_val);
extern void API_setDO_port(GPIO_TypeDef* GPIOx, uint16_t port_val);

extern void API_toggleDO_port(GPIO_TypeDef* GPIOx);

/**
  * @}
  */ 
	
/**
  * @}
  */ 
	
#endif // __USING_GPIO_H
