/**
  ******************************************************************************
  * @file    mcu.h
  * @author  Andreas Hirtenlehner
  * @brief   MCU definition
  */

#ifndef __MCU_H
#define __MCU_H

/** @addtogroup System_Core
  * @{
  */

/** @defgroup MCU 
  * @brief MCU selection
  * @{
  */

/* Exported constants --------------------------------------------------------*/
/** @defgroup MCU_Exported_Constants
  * @{
  */ 
	
#ifndef STM32F030
    #define STM32F030
#endif // !STM32F030

#define ARM_MATH_CM0

/**
  * @}
  */ 

/**
  * @}
  */ 

/**
  * @}
  */ 

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx.h"

/** @addtogroup System_Core
  * @{
  */

/** @addtogroup MCU
  * @{
  */

/* Exported types ------------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

/**
  * @}
  */ 

/**
  * @}
  */ 

#endif
