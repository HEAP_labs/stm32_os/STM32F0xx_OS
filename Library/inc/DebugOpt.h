#ifndef DEBUGOPT_H_INCLUDED
#define DEBUGOPT_H_INCLUDED


/*
   ---------------------------------------
   ---------- stdio semihosting ----------
   ---------------------------------------
*/
// not yet implemented
#define SYSTEM_STDIO_SEMIHOSTING          0


/*
   ---------------------------------------
   ---------- Debugging options ----------
   ---------------------------------------
*/
/**
 * SYSTEM_DBG_MIN_LEVEL: After masking, the value of the debug is
 * compared against this value. If it is smaller, then debugging
 * messages are written.
 */
#ifndef SYSTEM_DBG_MIN_LEVEL
#define SYSTEM_DBG_MIN_LEVEL              SYSTEM_DBG_LEVEL_ALL
#endif

/**
 * SYSTEM_DBG_TYPES_ON: A mask that can be used to globally enable/disable
 * debug messages of certain types.
 */
#ifndef SYSTEM_DBG_TYPES_ON
#define SYSTEM_DBG_TYPES_ON               SYSTEM_DBG_ON
#endif

/**
 * SYSCALL_DEBUG: Enable debugging in syscalls.c.
 */
#ifndef SYSCALL_DEBUG
#define SYSCALL_DEBUG                     SYSTEM_DBG_ON
#endif

/**
 * SIO_DEBUG: Enable debugging in sio.c.
 */
#ifndef SIO_DEBUG
#define SIO_DEBUG                     SYSTEM_DBG_ON
#endif

/**
 * HARDFAULT_DEBUG: Enable debugging in hard fault handler.
 */
#ifndef HARDFAULT_DEBUG
#define HARDFAULT_DEBUG                   SYSTEM_DBG_ON
#endif


/**
 * ZEITERFASSUNG_DEBUG: Enable debugging in "Zeiterfassung" application.
 */
#ifndef ZEITERFASSUNG_DEBUG
#define ZEITERFASSUNG_DEBUG                   SYSTEM_DBG_ON
#endif


#endif /* DEBUGOPT_H_INCLUDED */
