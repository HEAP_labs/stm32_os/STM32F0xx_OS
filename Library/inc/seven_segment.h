/**
* @file    ./Library/inc/seven_segment.h
* @author  Andreas Hirtenlehner
* @brief   Header for seven_segment.c module
*/

#ifndef __HW_SEVEN_SEGMENT_H
#define __HW_SEVEN_SEGMENT_H

/* Includes ------------------------------------------------------------------*/
#include "mcu.h"
#include "PerUsings.h"
#include "GPIO.h"
#include <stdarg.h>

#ifdef __USING_SPI
  #include "SPI.h"
#endif // __USING_SPI

/** @addtogroup MCU_Library
  * @{
  */

/** @addtogroup seven_segment
  * @{
  */

/* Exported types ------------------------------------------------------------*/

/** 
  * @brief  aligment of the displayed digits
  */
	
typedef enum LIB_7S_alignment_e
{
  RIGHT = 0,
  CENTER,
  LEFT,
} LIB_alignment_t;

/** 
  * @brief  common connection of the segments
  */

typedef enum LIB_7S_LED_configuration_e
{
  COMMON_ANODE = 0,
  COMMON_CATHODE,
} LIB_7S_LED_configuration_t;

/** 
  * @brief  hardware parameters of the seven segment display
  */
typedef struct LIB_7S_display_assembly_s
{
  uint8_t digit_count;
  LIB_7S_LED_configuration_t led_config;
} LIB_7S_display_assembly_t;

/** 
  * @brief  bit position of the segments in the SPI output data
  */
typedef struct LIB_7S_segment_bit_position_s
{
  uint8_t a;
  uint8_t b;
  uint8_t c;
  uint8_t d;
  uint8_t e;
  uint8_t f;
  uint8_t g;
  uint8_t dp;
} LIB_7S_segment_bit_position_t;

/** 
  * @brief  portpins of the segments when using parallel excitation
  */
typedef struct LIB_7S_segment_portpin_s
{
  API_GPIO_type_t* a;
  API_GPIO_type_t* b;
  API_GPIO_type_t* c;
  API_GPIO_type_t* d;
  API_GPIO_type_t* e;
  API_GPIO_type_t* f;
  API_GPIO_type_t* g;
  API_GPIO_type_t* dp;
} LIB_7S_segment_portpin_t;

/** 
  * @brief  SPI communication parameters
  */
typedef struct LIB_7S_SPI_communication_s
{
	API_SPI_type_t* API_SPIx;
	API_TIM_type_t* API_TIMx;
  API_GPIO_type_t* data_out;
  API_GPIO_type_t* clock;
  API_GPIO_type_t* le;
  API_GPIO_type_t* n_oe;
} LIB_7S_SPI_communication_t;

/** 
  * @brief  format of the displayed digits
  */
typedef struct LIB_7S_format_s
{
  uint8_t leading_zeros;
  uint8_t decimal_places;
  LIB_alignment_t alignment;
  uint8_t indention;
} LIB_7S_format_t;

/** 
  * @brief  lookup table for common characters
  */
typedef struct LIB_7S_character_s
{
  uint8_t numeral[10];
  uint8_t A;
  uint8_t b;
  uint8_t C;
  uint8_t c;
  uint8_t d;
  uint8_t E;
  uint8_t F;
  uint8_t G;
  uint8_t H;
  uint8_t h;
  uint8_t I;
  uint8_t i;
  uint8_t J;
  uint8_t j;
  uint8_t L;
  uint8_t n;
  uint8_t o;
  uint8_t r;
  uint8_t t;
  uint8_t U;
  uint8_t u;
  uint8_t minus;
  uint8_t underscore;
  uint8_t point;
} LIB_7S_character_t;

extern LIB_7S_character_t characters;

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern void LIB_7S_set_display_assembly(uint8_t digit_count, LIB_7S_LED_configuration_t led_config);
extern void LIB_7S_set_segment_bit_position(uint8_t a, uint8_t b, uint8_t c, uint8_t d, uint8_t e, uint8_t f, uint8_t g, uint8_t dp);
extern void LIB_7S_set_segment_portpin(API_GPIO_type_t* a, API_GPIO_type_t* b, API_GPIO_type_t* c, API_GPIO_type_t* d, API_GPIO_type_t* e, API_GPIO_type_t* f, API_GPIO_type_t* g, API_GPIO_type_t* dp);
extern void LIB_7S_set_digit_portpin(uint8_t count, ... );
extern void LIB_7S_set_format(uint8_t leading_zeros, uint8_t decimal_places, LIB_alignment_t alignment, uint8_t indention);
extern void LIB_7S_set_brightness(API_TIM_type_t* API_TIMx, uint8_t CHx, double duty_cycle);
extern void LIB_7S_digit_increment(void);
extern void LIB_7S_GPIO_show_char(uint8_t character);
extern void LIB_7S_GPIO_clear(void);
extern void LIB_7S_SPI_init(API_SPI_type_t* API_SPIx, API_GPIO_type_t* mosi_portpin, API_GPIO_type_t* sck_portpin, API_GPIO_type_t* le_portpin, API_GPIO_type_t* n_oe_portpin, uint16_t baudrate_prescaler, uint8_t mode);
extern void LIB_7S_SPI_DMA_TIM_start_continious_tx(API_TIM_type_t* API_TIMx, double t_period_s);
extern void LIB_7S_SPI_DMA_TIM_stop_continious_tx(void);
extern void LIB_7S_SPI_DMA_TIM_show_number(double number);
extern void LIB_7S_SPI_DMA_TIM_show_string(uint8_t *p_characters);
extern void LIB_7S_SPI_clear(void);

/**
  * @}
  */ 
	
/**
  * @}
  */ 

#endif // __HW_SEVEN_SEGMENT_H
