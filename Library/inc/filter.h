/**
  * @file    ./Library/inc/filter.h
  * @author  Andreas Hirtenlehner
  * @brief   Header for filter module
  */
  
#ifndef __FILTER_H
#define __FILTER_H

/* Includes ------------------------------------------------------------------*/
#include "mcu.h"
#include "arm_math.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

#endif // __HW_SIGNAL_READ_H
