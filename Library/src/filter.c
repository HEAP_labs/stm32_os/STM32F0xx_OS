/**
 * @file    ./Library/src/filter.c
 * @author  Andreas Hirtenlehner
 * @brief   filter an analog signal
 */

#include "LibUsings.h"
#ifdef __USING_FILTER

#include "filter.h"

void LIB_FILTER_q15_iir(uint32_t* x_n[], uint32_t* y_n[], uint16_t n_index, uint16_t filter_order, double* a[], double* b[])
{
	uint16_t i = 0;
	
	*y_n[n_index] = 0;
	
	for(i = 0; i < filter_order+1; i++)
	{
		*y_n[n_index] += *b[n_index+i] * *x_n[n_index-i] - *a[n_index+i] * *y_n[n_index-i];
	}
}

void LIB_FILTER_q15_iir_sos(uint32_t* x_n[], uint32_t* y_n[], uint16_t n_index, uint16_t filter_order, double* a[], double* b[])

void LIB_FILTER_q15_fir(uint32_t* x_n[], uint32_t* y_n[], uint16_t n_index, uint16_t filter_order, double* a[], double* b[])
{
	uint16_t i = 0;
	arm_status status;
	
	status = arm_fir_init_q15(
	
	*y_n[n_index] = 0;
	
	for(i = 0; i < filter_order+1; i++)
	{
		*y_n[n_index] += *b[n_index+i] * *x_n[n_index-i] - *a[n_index+i] * *y_n[n_index-i];
	}
}

#endif // __USING_FILTER
